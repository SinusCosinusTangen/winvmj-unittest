package seleniumTest.orm;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class FrontEndTest {
    private static WebDriver driver;

    @BeforeAll
    public static void setUpAll() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("allure", new AllureSelenide());
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver.get("http://localhost:3000");
        login();
    }

    @AfterAll
    public static void cleanUp() {
        //driver.close();
    }

    public static void login() {
        driver.findElement(By.linkText("MASUK")).click();
        WebElement emailElement = driver.findElement(By.name("email"));
        emailElement.sendKeys("jamestesting@gmail.com");
        WebElement passwordElement = driver.findElement(By.name("password"));
        passwordElement.sendKeys("testing123");
        sleep(5000);
    }

    @DisplayName("Find Activity")
    @Test
    public void findActivity() {

        sleep(2000);
        driver.findElement(By.xpath("//button[text()='Program']")).click();
        driver.findElement(By.linkText("Aktivitas")).click();
        sleep(2000);
        driver.findElements(By.linkText("DETAIL")).get(0).click();
        sleep(2000);

        List<WebElement> listElement = new ArrayList<>();
        List<String> listResultString = new ArrayList<>();
        List<String> listExpectedString = List.of("NAMA", "DESKRIPSI", "TARGET", "PARTNER", "TANGGAL PELAKSANAAN");

        for (String expected : listExpectedString) {
            listElement.addAll(driver.findElements(By.xpath("//*[contains(text(), expected)]")));
        }

        for (WebElement element : listElement) {
            listResultString.add(element.getText());
        }

        Assertions.assertTrue(listResultString.containsAll(listExpectedString));
    }

    @DisplayName("Find Expenses")
    @Test
    public void findExpenses() {

        sleep(2000);
        driver.findElement(By.xpath("//button[text()='Laporan Keuangan']")).click();
        driver.findElement(By.linkText("Pengeluaran")).click();
        sleep(2000);
        driver.findElements(By.linkText("DETAIL")).get(0).click();
        sleep(2000);

        List<WebElement> listElement = new ArrayList<>();
        List<String> listResultString = new ArrayList<>();
        List<String> listExpectedString = List.of("NAMA PROGRAM", "DESKRIPSI", "JUMLAH", "JENIS PENGELUARAN", "TANGGAL");

        for (String expected : listExpectedString) {
            listElement.addAll(driver.findElements(By.xpath("//*[contains(text(), expected)]")));
        }

        for (WebElement element : listElement) {
            listResultString.add(element.getText());
        }

        Assertions.assertTrue(listResultString.containsAll(listExpectedString));
    }

    @DisplayName("Find Donation Confirmation")
    @Test
    public void findDonationConfirmation() {

        sleep(2000);
        driver.findElement(By.xpath("//button[text()='Donasi']")).click();
        driver.findElement(By.linkText("Konfirmasi")).click();
        sleep(2000);
        driver.findElements(By.linkText("DETAIL")).get(0).click();
        sleep(2000);

        List<WebElement> listElement = new ArrayList<>();
        List<String> listResultString = new ArrayList<>();
        List<String> listExpectedString = List.of("NAMA", "EMAIL", "NO. TELP", "TANGGAL", "JUMLAH", "DESKRIPSI", "METODE PEMBAYARAN", "STATUS");

        for (String expected : listExpectedString) {
            listElement.addAll(driver.findElements(By.xpath("//*[contains(text(), expected)]")));
        }

        for (WebElement element : listElement) {
            listResultString.add(element.getText());
        }

        Assertions.assertTrue(listResultString.containsAll(listExpectedString));
    }

    @Test
    public void insertProgram() {

        sleep(2000);
        driver.findElement(By.xpath("//button[text()='Program']")).click();
        driver.findElement(By.linkText("Aktivitas")).click();
        sleep(2000);
        driver.findElement(By.linkText("TAMBAH PROGRAM")).click();
        sleep(2000);

        WebElement name = driver.findElement(By.name("name"));
        WebElement description = driver.findElement(By.name("description"));
        WebElement target = driver.findElement(By.name("target"));        WebElement partner = driver.findElement(By.name("partner"));
        WebElement executionDate = driver.findElement(By.name("executionDate"));
        WebElement logoUrl = driver.findElement(By.name("logoUrl"));

        sleep(1000);

        name.sendKeys("Testing Program");
        sleep(1000);
        description.sendKeys("Testing Description");
        sleep(1000);
        target.sendKeys("Testing Target");
        sleep(1000);
        partner.sendKeys("Testing Partner");
        sleep(1000);
        executionDate.sendKeys("2023/03/12");
        sleep(1000);
        logoUrl.sendKeys("https://awsimages.detik.net.id/community/media/visual/2021/12/08/otter-1_169.jpeg?w=1200");
        sleep(1000);

        driver.findElement(By.linkText("KIRIM")).click();
//        sleep(2000);
    }

//    @Test
//    public void toolsMenu() {
//        mainPage.toolsMenu.click();
//
//        $("div[data-test='main-submenu']").shouldBe(visible);
//    }
//
//    @Test
//    public void navigationToAllTools() {
//        mainPage.seeDeveloperToolsButton.click();
//        mainPage.findYourToolsButton.click();
//
//        $("#products-page").shouldBe(visible);
//
//        assertEquals("All Developer Tools and Products by JetBrains", Selenide.title());
//    }
}
