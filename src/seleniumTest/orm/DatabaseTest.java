package seleniumTest.orm;

import org.junit.jupiter.api.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseTest {

    private static Connection con;
    private static String dbUrl;
    private static String username;
    private static String password;

    @BeforeAll
    public static void setUpAll() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        dbUrl = "jdbc:postgresql://localhost:5432/aisco_product_berjayabersama";
        username = "postgres";
        password = "postgres";
        con = DriverManager.getConnection(dbUrl, username, password);
    }

    @BeforeEach
    public void breakBefore() {
        System.out.println();
    }

    @AfterAll
    public static void cleanUpAll() throws SQLException {
        con.close();
    }

    @DisplayName("Test program_impl Table Attribute")
    @Test
    public void testProgramImplTable() throws SQLException {

        String query = "SELECT * FROM program_impl";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of(
                "description", "executiondate", "logourl", "name", "partner", "target", "idprogram"
        );
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== program_impl ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test program_comp Table Attribute")
    @Test
    public void testProgramCompTable() throws SQLException {

        String query = "SELECT * FROM program_comp";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of("idprogram");
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== program_comp ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test donation_comp Table Attribute")
    @Test
    public void testDonationCompTable() throws SQLException {

        String query = "SELECT * FROM donation_comp";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of("id");
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== donation_impl ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test donation_impl Table Attribute")
    @Test
    public void testDonationImplTable() throws SQLException {

        String query = "SELECT * FROM donation_impl";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of(
                "amount", "date", "description", "email", "name", "paymentmethod",
                "phone", "id", "income_id", "program_idprogram"
        );
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== donation_impl ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test donation_confirmation Table Attribute")
    @Test
    public void testDonationConfirmationTable() throws SQLException {

        String query = "SELECT * FROM donation_confirmation";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of(
                "proofoftransfer", "recieveraccount", "senderaccount",
                "status", "id", "record_id"
        );
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== donation_confirmation ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test financialreport_comp Table Attribute")
    @Test
    public void testFinancialReportCompTable() throws SQLException {

        String query = "SELECT * FROM financialreport_comp";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of("id");
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== fincancialreport_comp ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }

    @DisplayName("Test financialreport_impl Table Attribute")
    @Test
    public void testFinancialReportImplTable() throws SQLException {

        String query = "SELECT * FROM financialreport_impl";
        Statement stmt = con.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);

        List<String> expectedColumn = List.of("amount", "datestamp", "description", "id", "coa_id", "program_idprogram");
        List<String> actualColumn = new ArrayList<>();

        if (resultSet.next()) {
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                actualColumn.add(resultSet.getMetaData().getColumnName(i));
            }
        }

        System.out.println("=== financialreport_impl ===");
        System.out.println("Expected Column: " + expectedColumn);
        System.out.println("Actual Column:   " + actualColumn);

        Assertions.assertTrue(expectedColumn.containsAll(actualColumn));
        Assertions.assertTrue(actualColumn.containsAll(expectedColumn));
    }
}