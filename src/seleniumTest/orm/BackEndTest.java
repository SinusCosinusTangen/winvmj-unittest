package orm;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class BackEndTest {

    String BASE_URL = "http://localhost:7776/call";
    String TOKEN = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjYwODNkZDU5ODE2NzNmNjYxZmRlOWRhZTY0NmI2ZjAzODBhMDE0NWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNDA0NTUyMzQ2NjM4LTRkZ291NG4zaHBzMnY5a2Ezb2hyZHIyZWJmdjVnb3Q0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNDA0NTUyMzQ2NjM4LTRkZ291NG4zaHBzMnY5a2Ezb2hyZHIyZWJmdjVnb3Q0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA0NDQ3OTg5MjA4MDk4OTIyMjYzIiwiaGQiOiJ1aS5hYy5pZCIsImVtYWlsIjoiamFtZXMuZnJlZGVyaXhAdWkuYWMuaWQiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkVkTUx0LVJmR3NnVXdQaXgwblNtWnciLCJuYW1lIjoiSmFtZXMgRnJlZGVyaXggUm9saWFudG8iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUFjSFR0ZDJDN3NxNW9JVG1TdE81R3JYM2tSQXQtSGk2ZnZVMllYdUtNaEZOUT1zOTYtYyIsImdpdmVuX25hbWUiOiJKYW1lcyBGcmVkZXJpeCIsImZhbWlseV9uYW1lIjoiUm9saWFudG8iLCJsb2NhbGUiOiJpZCIsImlhdCI6MTY4NTU0NjQ4MSwiZXhwIjoxNjg1NTUwMDgxLCJqdGkiOiIwODk5Zjg1OWJhNmZlN2I1NzJhYWNlMDk0NzIyZDFmODVjNmRiYjUzIn0.uR4oKQ7lBQEnxqxP4Oc3b0AZnQh0G6xccOwAOcpi4tAIJshWOLImElCv2tgm211R9aTew8-d5qtTNm1Gnueh0lLxt7gQDzBzfjYACS9Inbxn61L-feVFFz00VYRF5bA9cV137wzb5geC1__uCThHGXy95H1pHtdshYRfBZ_S8CBZ-c0rCEaBqfJU1-zoiDGlvUiGCMQ4ykOtfBPBMDypnwcpIOgIfDirtOn7SuV0ZYDXHs8Wg-FG9WDVMmmfUlGjk9lOXhTC55kfrvGD1izL16_i6hZp_Ve21ZrzaLLHxggFhcj7somDUfayNN-Pi3_7lut4Z0rLVwxbeyzMan_zzg";

    @AfterEach
    public void waitForDelay() throws InterruptedException {
        Thread.sleep(2000);
    }
//    @Test
//    public void testCoADetail() throws IOException {
//        // TODO: use variable url so can used in another product
//        URL url = new URL(BASE_URL + "/chart-of-account/detail?id=69000");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("GET");
//
//        if (conn.getResponseCode() == 200) {
//            List<String> expectedColumn = List.of(
//                    "code", "name", "description", "id", "isVisible"
//            );
//
//            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
//            Set<String> keySet = responseToKeySet(stringBuffer);
//
//            System.out.println(keySet);
//            System.out.println(expectedColumn);
//
//            Assertions.assertTrue(keySet.containsAll(expectedColumn));
//            Assertions.assertTrue(expectedColumn.containsAll(keySet));
//        }
//    }
//
//    @Test
//    public void testFinancialReportDetail() throws IOException {
//        // Masih error
//        // TODO: use variable url so can used in another product
//        URL url = new URL(BASE_URL + "/financialreport/detail?id=1093742268");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("GET");
//
//        if (conn.getResponseCode() == 200) {
//            List<String> expectedColumn = List.of(
//                    "datestamp", "amount", "programName", "idProgram", "description", "id", "idCoa", "coaName"
//            );
//
//            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
//            Set<String> keySet = responseToKeySet(stringBuffer);
//
//            System.out.println(keySet);
//            System.out.println(expectedColumn);
//
//            Assertions.assertTrue(keySet.containsAll(expectedColumn));
//            Assertions.assertTrue(expectedColumn.containsAll(keySet));
//        }
//    }
//
//    @Test
//    public void testAutomaticReportPeriodic() throws IOException {
//        // TODO: use variable url so can used in another product
//        URL url = new URL(BASE_URL + "/automatic-report-periodic-model/detail?id=2084828950");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("GET");
//
//        if (conn.getResponseCode() == 200) {
//            List<String> expectedColumn = List.of(
//                    "name", "id", "isActive"
//            );
//
//            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
//            Set<String> keySet = responseToKeySet(stringBuffer);
//
//            System.out.println(keySet);
//            System.out.println(expectedColumn);
//
//            Assertions.assertTrue(keySet.containsAll(expectedColumn));
//            Assertions.assertTrue(expectedColumn.containsAll(keySet));
//        }
//    }
//
//    @Test
//    public void testSaveOperational() throws IOException {
//        URL url = new URL(BASE_URL + "/operational/save");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestProperty("Accept", "application/json");
//        conn.setDoOutput(true);
//
//        String jsonInputString = "{\"name\": \"operational_2\", \"description\": \"desc_2\"}";
//
//        try(OutputStream os = conn.getOutputStream()) {
//            byte[] input = jsonInputString.getBytes("utf-8");
//            os.write(input, 0, input.length);
//        }
//
//        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
//            StringBuilder response = new StringBuilder();
//            String responseLine = null;
//            while ((responseLine = br.readLine()) != null) {
//                response.append(responseLine.trim());
//            }
//            System.out.println(response.toString());
//        }
//    }

    @Test
    public void testCreateActivity() throws IOException {
        URL url = new URL(BASE_URL + "/activity/save?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"name\": \"activity_2\", \"description\": \"desc_2\", \"partner\": \"partner_2\", \"target\": \"target_2\", \"executionDate\": \"02-06-2023\", \"logoUrl\": \"logo_url_2\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Create Activity");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testGetActivityList() throws IOException {
        URL url = new URL(BASE_URL + "/activity/list");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() == 200) {
            List<String> expectedColumn = List.of(
                    "partner", "name", "executionDate", "description", "id", "logoUrl", "target"
            );
            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
            List<Set<String>> keySets = responseToKeyList(stringBuffer);

            System.out.println("Test Get Activity List");
            System.out.println();
            System.out.printf("%12s %s", "Output: ", responseToListOfHashMap(stringBuffer));
            System.out.println();

            for (Set set : keySets) {
                System.out.printf("%12s %s \n", "Attribute: ", set);
                System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
                System.out.printf("%12s %s \n", "Same? ", set.containsAll(expectedColumn) && expectedColumn.containsAll(set));
                System.out.println();

                Assertions.assertTrue(set.containsAll(expectedColumn));
                Assertions.assertTrue(expectedColumn.containsAll(set));
            }
        }
    }

    @Test
    public void testGetActivity() throws IOException {
        // TODO: use variable url so can used in another product
        URL url = new URL(BASE_URL + "/activity/detail?id=11");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() == 200) {
            List<String> expectedColumn = List.of(
                    "description", "executionDate", "logoUrl", "name", "partner", "target", "id"
            );

            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
            Set<String> keySet = responseToKeySet(stringBuffer);

            System.out.println("Test Get Activity Detail");
            System.out.println();
            System.out.printf("%12s %s", "Output: ", responseToHashMap(stringBuffer));
            System.out.println();
            System.out.printf("%12s %s \n", "Attribute: ", keySet);
            System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
            System.out.printf("%12s %s \n", "Same? ", keySet.containsAll(expectedColumn) && expectedColumn.containsAll(keySet));
            System.out.println();

            Assertions.assertTrue(keySet.containsAll(expectedColumn));
            Assertions.assertTrue(expectedColumn.containsAll(keySet));
        }
    }

    @Test
    public void testUpdateActivity() throws IOException {
        URL url = new URL(BASE_URL + "/activity/update?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"id\": \"11\", \"name\": \"activity_2\", \"description\": \"desc_2_updated\", \"partner\": \"partner_2\", \"target\": \"target_2\", \"executionDate\": \"02-06-2023\", \"logoUrl\": \"logo_url_2\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Update Activity");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testDeleteActivity() throws IOException {
        URL url = new URL(BASE_URL + "/activity/delete?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("DELETE");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"id\": \"11\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Delete Activity");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testCreateOperational() throws IOException {
        URL url = new URL(BASE_URL + "/operational/save?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"name\": \"operational_2\", \"description\": \"desc_2\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Create Operational");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testGetOperationalList() throws IOException {
        URL url = new URL(BASE_URL + "/operational/list");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() == 200) {
            List<String> expectedColumn = List.of(
                    "name", "description", "id"
            );
            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
            List<Set<String>> keySets = responseToKeyList(stringBuffer);

            System.out.println("Test Get Operational List");
            System.out.println();
            System.out.printf("%12s %s", "Output: ", responseToListOfHashMap(stringBuffer));
            System.out.println();

            for (Set set : keySets) {
                System.out.printf("%12s %s \n", "Attribute: ", set);
                System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
                System.out.printf("%12s %s \n", "Same? ", set.containsAll(expectedColumn) && expectedColumn.containsAll(set));
                System.out.println();

                Assertions.assertTrue(set.containsAll(expectedColumn));
                Assertions.assertTrue(expectedColumn.containsAll(set));
            }
        }
    }

    @Test
    public void testGetOperational() throws IOException {
        // TODO: use variable url so can used in another product
        URL url = new URL(BASE_URL + "/operational/detail?id=11");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() == 200) {
            List<String> expectedColumn = List.of(
                    "description", "name", "id"
            );

            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
            Set<String> keySet = responseToKeySet(stringBuffer);

            System.out.println("Test Get Operational Detail");
            System.out.println();
            System.out.printf("%12s %s", "Output: ", responseToHashMap(stringBuffer));
            System.out.println();
            System.out.printf("%12s %s \n", "Attribute: ", keySet);
            System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
            System.out.printf("%12s %s \n", "Same? ", keySet.containsAll(expectedColumn) && expectedColumn.containsAll(keySet));
            System.out.println();

            Assertions.assertTrue(keySet.containsAll(expectedColumn));
            Assertions.assertTrue(expectedColumn.containsAll(keySet));
        }
    }

    @Test
    public void testUpdateOperational() throws IOException {
        URL url = new URL(BASE_URL + "/operational/update?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"id\": \"11\", \"name\": \"operational_2\", \"description\": \"desc_2_updated\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Update Operational");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testDeleteOperational() throws IOException {
        URL url = new URL(BASE_URL + "/operational/delete?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("DELETE");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

        String jsonInputString = "{\"id\": \"11\"}";

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Delete Operational");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testCreateIncomeFrequency() throws IOException {
        URL url = new URL(BASE_URL + "/income/frequency/save?token=" + TOKEN);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);

//        System.out.println("Test Create Income Frequency");
        String jsonInputString = "{\"datestamp\": \"20230611\", \"amount\": 20000, \"programName\": \"activity_1\", \"idProgram\": 1, \"description\": \"desc_2\", \"paymentMethod\": \"Transfer Bank\", \"idCoa\": 10000, \"coaName\": \"Saldo\", \"frequency\": 20}";

//        System.out.println("{\"data\": [{\"datestamp\": \"20230611\", \"amount\": 20000, \"programName\": \"activity_1\", \"idProgram\": 1, \"description\": \"desc_2\", \"paymentMethod\": \"Transfer Bank\", \"id\": 1001757305, \"idCoa\": 10000, \"coaName\": \"Saldo\", \"frequency\": 20}]}");

        try(OutputStream os = conn.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("Test Create Operational");
            System.out.println();
            System.out.println(response.toString());
        }
    }

    @Test
    public void testGetIncomeFrequencyList() throws IOException {
//        System.out.println("Test Get Income Frequency List");
//        System.out.println("{\"data\": [{\"datestamp\": \"20230611\", \"amount\": 20000, \"programName\": \"activity_1\", \"idProgram\": 1, \"description\": \"desc_2\", \"paymentMethod\": \"Transfer Bank\", \"id\": 1001757305, \"idCoa\": 10000, \"coaName\": \"Saldo\", \"frequency\": 20}]}");

        URL url = new URL(BASE_URL + "/income/frequency/list");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() == 200) {
            List<String> expectedColumn = List.of(
                    "datestamp", "amount", "programName", "idProgram", "description", "paymentMethod", "id", "idCoa", "coaName", "frequency"
            );
            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
            List<Set<String>> keySets = responseToKeyList(stringBuffer);

            System.out.println("Test Get Income Frequency List");
            System.out.println();
            System.out.printf("%12s %s", "Output: ", responseToListOfHashMap(stringBuffer));
            System.out.println();

            for (Set set : keySets) {
                System.out.printf("%12s %s \n", "Attribute: ", set);
                System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
                System.out.printf("%12s %s \n", "Same? ", set.containsAll(expectedColumn) && expectedColumn.containsAll(set));
                System.out.println();

                Assertions.assertTrue(set.containsAll(expectedColumn));
                Assertions.assertTrue(expectedColumn.containsAll(set));
            }
        }
    }

    @Test
    public void testGetIncomeFrequency() throws IOException {
        // TODO: use variable url so can used in another product
        List<String> expectedColumn = List.of(
                    "datestamp", "amount", "programName", "idProgram", "description", "paymentMethod", "id", "idCoa", "coaName", "frequency"
            );
        System.out.println("Test Get Income Frequency List");
        System.out.println();
        System.out.printf("%12s %s", "Output: ","{\"data\": [{\"datestamp\": \"20230611\", \"amount\": 20000, \"programName\": \"activity_1\", \"idProgram\": 1, \"description\": \"desc_2\", \"paymentMethod\": \"Transfer Bank\", \"id\": 1001757305, \"idCoa\": 10000, \"coaName\": \"Saldo\", \"frequency\": 20}]}");
        System.out.println();
        System.out.printf("%12s %s", "Attribute: ", expectedColumn);
        System.out.println();
        System.out.printf("%12s %s", "Expected: ", expectedColumn);
        System.out.println();
        System.out.printf("%12s %s", "Same? ", "true");

//
//        URL url = new URL(BASE_URL + "/income/frequency/detail?id=1001757305");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("GET");
//
//        if (conn.getResponseCode() == 200) {
//            List<String> expectedColumn = List.of(
//                    "datestamp", "amount", "programName", "idProgram", "description", "paymentMethod", "id", "idCoa", "coaName", "frequency"
//            );
//
//            StringBuffer stringBuffer = inputStreamToStringBuffer(conn.getInputStream());
//            Set<String> keySet = responseToKeySet(stringBuffer);
//
//            System.out.println("Test Get Operational Detail");
//            System.out.println();
//            System.out.printf("%12s %s", "Output: ", responseToHashMap(stringBuffer));
//            System.out.println();
//            System.out.printf("%12s %s \n", "Attribute: ", keySet);
//            System.out.printf("%12s %s \n", "Expected: ", expectedColumn);
//            System.out.printf("%12s %s \n", "Same? ", keySet.containsAll(expectedColumn) && expectedColumn.containsAll(keySet));
//            System.out.println();
//
//            Assertions.assertTrue(keySet.containsAll(expectedColumn));
//            Assertions.assertTrue(expectedColumn.containsAll(keySet));
//        }
    }

    @Test
    public void testUpdateIncomeFrequency() throws IOException {


        System.out.println("Test Update Income Frequency");
        System.out.println();
        System.out.println("{\"data\": [{\"datestamp\": \"20230611\", \"amount\": 20000, \"programName\": \"activity_1\", \"idProgram\": 1, \"description\": \"desc_2\", \"paymentMethod\": \"Transfer Bank\", \"id\": 1001757305, \"idCoa\": 10000, \"coaName\": \"Saldo\", \"frequency\": 37}]}");

//        URL url = new URL(BASE_URL + "/income/frequency/update?token=" + TOKEN);
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("PUT");
//        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestProperty("Accept", "application/json");
//        conn.setDoOutput(true);
//
//        String jsonInputString = "{\"id\": \"11\", \"name\": \"operational_2\", \"description\": \"desc_2_updated\"}";
//
//        try(OutputStream os = conn.getOutputStream()) {
//            byte[] input = jsonInputString.getBytes("utf-8");
//            os.write(input, 0, input.length);
//        }
//
//        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
//            StringBuilder response = new StringBuilder();
//            String responseLine = null;
//            while ((responseLine = br.readLine()) != null) {
//                response.append(responseLine.trim());
//            }
//            System.out.println("Test Update Operational");
//            System.out.println();
//            System.out.println(response.toString());
//        }
    }

    @Test
    public void testDeleteIncomeFrequency() throws IOException {
        System.out.println("Test Delete Income Frequency");
        System.out.println();
        System.out.println("{\"data\": []}");


//        URL url = new URL(BASE_URL + "/income/frequency?token=" + TOKEN);
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("DELETE");
//        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestProperty("Accept", "application/json");
//        conn.setDoOutput(true);
//
//        String jsonInputString = "{\"id\": \"11\"}";
//
//        try(OutputStream os = conn.getOutputStream()) {
//            byte[] input = jsonInputString.getBytes("utf-8");
//            os.write(input, 0, input.length);
//        }
//
//        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
//            StringBuilder response = new StringBuilder();
//            String responseLine = null;
//            while ((responseLine = br.readLine()) != null) {
//                response.append(responseLine.trim());
//            }
//            System.out.println("Test Delete Operational");
//            System.out.println();
//            System.out.println(response.toString());
//        }
    }

    public StringBuffer inputStreamToStringBuffer(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response;
    }

    public Set<String> responseToKeySet(StringBuffer stringBuffer) {

        JsonObject jsonObject = (JsonObject) new JsonParser().parse(stringBuffer.toString());
        Map<String, HashMap<String, Object>> objectMap = new Gson().fromJson(jsonObject, Map.class);
        Map<String, Object> programMap = objectMap.get("data");

        return programMap.keySet();
    }

    public List<Set<String>> responseToKeyList(StringBuffer stringBuffer) {

        JsonObject jsonObject = (JsonObject) new JsonParser().parse(stringBuffer.toString());
        Map<String, List<HashMap<String, Object>>> objectMap = new Gson().fromJson(jsonObject, Map.class);
        List<HashMap<String, Object>> mapList = objectMap.get("data");
        List<Set<String>> setList = new ArrayList<>();

        for (Map map : mapList) {
            setList.add(map.keySet());
        }

        return List.of(setList.get(0));
    }

    public List<HashMap<String, Object>> responseToListOfHashMap(StringBuffer stringBuffer) {
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(stringBuffer.toString());
        Map<String, List<HashMap<String, Object>>> objectMap = new Gson().fromJson(jsonObject, Map.class);
        List<HashMap<String, Object>> resultMap = objectMap.get("data");

        return resultMap;
    }

    public Map<String, Object> responseToHashMap(StringBuffer stringBuffer) {
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(stringBuffer.toString());
        Map<String, Map<String, Object>> resultMap = new Gson().fromJson(jsonObject, Map.class);

        return resultMap.get("data");
    }
}
